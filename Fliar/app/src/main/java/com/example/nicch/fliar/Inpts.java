package com.example.nicch.fliar;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Inpts extends AppCompatActivity {

    Calendar cal=new GregorianCalendar();
    EditText mun,val1,val2,val3;
    Button sav,term;
    AlertDialog.Builder Onyo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inpts);

        //String moon;
        sav=(Button)findViewById(R.id.btnSav);
        term= (Button) findViewById(R.id.btnTerm);
        val1= (EditText) findViewById(R.id.etInp);
        val2= (EditText) findViewById(R.id.etSpec);
        val3= (EditText) findViewById(R.id.etCostar);
        mun= (EditText) findViewById(R.id.edMun);
        int dy=cal.get(Calendar.DAY_OF_MONTH);
        int dy2=cal.get(Calendar.MONTH);
        int dy3=cal.get(Calendar.YEAR);

        mun.setEnabled(Boolean.FALSE);

        val1.requestFocus();

        mun.setText(String.valueOf(dy)+"--"+String.valueOf(dy2)+"--"+String.valueOf(dy3));
        sav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String var1,var2,var3;

                var1=String.valueOf(val1.getText());
                var2=String.valueOf(val2.getText());
                var3=String.valueOf(val3.getText());

                Onyo=new AlertDialog.Builder(Inpts.this);
                //Onyo.setMessage("Inputs->\nSpecification->\nCost->");
                Onyo.setMessage("Inputs->"+var1+"\nSpecification->"+var2+"\nCost->"+var3);
                Onyo.setPositiveButton("Continue",null);
                Onyo.setNegativeButton("Retry",null);
                Onyo.create();
                Onyo.show();

                Toast.makeText(Inpts.this,"*//*",Toast.LENGTH_SHORT).show();

                Fragment frg=null;
                Class FrgCla=null;
                FrgCla=ListaPrjs.class;
                try {
                    frg= (Fragment) FrgCla.newInstance();
                }catch (Exception e){
                    e.printStackTrace();
                }
                FragmentManager fm=getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.Uwanja,frg).commit();
            }
        });

        term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                startActivity(new Intent(Inpts.this,Logistics.class));
                //Smok();
            }
        });

    }

    public void Smok(){
        Onyo=new AlertDialog.Builder(this);
        Onyo.setMessage("--");
        Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(Inpts.this,Logistics.class));
                dialogInterface.dismiss();
            }
        });
    }
}
