package com.example.nicch.fliar;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Disp01 extends AppCompatActivity {

    Button bc,ed;
    TextView nam,id,ty,el,Aut,mp,pona,exp;
    Integer ff;
    String aa,bb,cc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disp01);

        ListIT lita=new ListIT(aa,bb,cc);

        //id.setText(aa+""+bb+""+cc+"");*/

        bc= (Button) findViewById(R.id.Backer);
        ed= (Button) findViewById(R.id.Edita);

        mp= (TextView) findViewById(R.id.morr);
        pona= (TextView) findViewById(R.id.PrOwna);
        ty= (TextView) findViewById(R.id.PrTypa);
        nam= (TextView) findViewById(R.id.PrNan);
        el= (TextView) findViewById(R.id.PrEL);
        Aut= (TextView) findViewById(R.id.PrAut);
        exp=(TextView) findViewById(R.id.prExpnd);

        Intent intt=getIntent();

        Bundle bud=getIntent().getExtras();
        //String va=bud.getString("Counta");

        //ty.setText(va);
        pona.setText(String.valueOf(intt.getStringExtra("Counta")));
        nam.setText(intt.getStringExtra("Counta"));
        el.setText(getIntent().getStringExtra("Counta"));
        Aut.setText("Hahhahah");

        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Disp01.this, Projctas.class));
            }
        });

        ed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Fragment frg=null;
                Class FrgCla=null;
                FrgCla=Edito.class;
                try {
                    frg= (Fragment) FrgCla.newInstance();
                }catch (Exception e){
                    e.printStackTrace();
                }
                FragmentManager fm=getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.Uwanja,frg).commit();*/

                startActivity(new Intent(Disp01.this,Inpts.class));
            }
        });

        mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Disp01.this,"Action Pending",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
