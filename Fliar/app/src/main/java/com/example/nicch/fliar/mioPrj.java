package com.example.nicch.fliar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class mioPrj extends Fragment {

    private RecyclerView rcVw;
    private  RecyclerView.Adapter RcVwAd;

    private List<ListIT> listITs;

    public mioPrj() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rutv= inflater.inflate(R.layout.frag_mio_prj, container, false);

        rcVw =(RecyclerView) rutv.findViewById(R.id.MyDropa);
        rcVw.setHasFixedSize(true);
        rcVw.setLayoutManager(new LinearLayoutManager(getContext()));

        listITs=new ArrayList<>();

        for (int i=0;i<=20;i++){
            ListIT lit=new ListIT(
                    "Element Count "+ (i+1),
                    "Label Next ",
                    "Hahah"
            );

            listITs.add(lit);

            rcVw.setAdapter(RcVwAd);
        }

        RcVwAd =new FellAdpter(listITs,getContext());

        rcVw.setAdapter(RcVwAd);

        return rutv;
    }

}
