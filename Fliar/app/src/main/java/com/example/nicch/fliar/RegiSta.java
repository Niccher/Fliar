package com.example.nicch.fliar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class RegiSta extends AppCompatActivity {
    private Button wrt;
    AlertDialog.Builder Onyo;
    Context cnt;

    //EditText nm,ml,Uns,Pw,cpw,pnn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activ_regi_sta);

        getSupportActionBar().setTitle("Create User");

        wrt= (Button) findViewById(R.id.Signpe);

        final EditText nm= (EditText) findViewById(R.id.etNam);
        final EditText pnn= (EditText) findViewById(R.id.etPNuber);
        final EditText Idh= (EditText) findViewById(R.id.etId);
        final EditText ml= (EditText) findViewById(R.id.etEml);
        final EditText Uns= (EditText) findViewById(R.id.etUsr);
        final EditText Pw= (EditText) findViewById(R.id.etPaswd);
        final EditText cpw= (EditText) findViewById(R.id.etCpd);

        wrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Andika();
                if(nm.getText().toString().equals("")||Idh.getText().toString().equals(null)||pnn.getText().toString().equals("")||Uns.getText().toString().equals("")||Pw.getText().toString().equals("")||ml.getText().toString().equals("")||cpw.getText().toString().equals("")){
                    Onyo=new AlertDialog.Builder(RegiSta.this);
                    Onyo.setTitle("Something went wrong..");
                    Onyo.setMessage("Please fill all the fields....");
                    Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog AllDg=Onyo.create();
                    AllDg.show();
                }
                else{
                    final String nms=nm.getText().toString();
                    final int Phon= Integer.parseInt(pnn.getText().toString());
                    final int IdS= Integer.parseInt(Idh.getText().toString());
                    final String eml=ml.getText().toString();
                    final String Usrn=Uns.getText().toString();
                    final String pwdd=Pw.getText().toString();
                    final String Cpwdd=cpw.getText().toString();

                    final ProgressDialog progressDialog=new ProgressDialog(RegiSta.this);
                    progressDialog.setMessage("Connecting to Servers...Please wait");

                    progressDialog.show();

                    if (pwdd.equals(Cpwdd)){
                        Response.Listener<String> responseListener=new Response.Listener<String>(){
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonResponse=new JSONObject(response);
                                    boolean success=jsonResponse.getBoolean("success");
                                        if(success){
                                            progressDialog.dismiss();
                                                startActivity(new Intent(RegiSta.this,MainActivity.class));
                                            Toast.makeText(RegiSta.this,"Your Username :"+Usrn+"\nPassword :"+pwdd,Toast.LENGTH_LONG).show();
                                        }
                                        else {
                                            AlertDialog.Builder builder=new AlertDialog.Builder(RegiSta.this);
                                            builder.setMessage("Registration failed");
                                            builder.setNegativeButton("Retry",null);
                                            builder.create();
                                            builder.show();
                                        }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(RegiSta.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                            //@Override
                            public void onErrorResponse(VolleyError er) {
                                Toast.makeText(RegiSta.this, er.getMessage(), Toast.LENGTH_SHORT).show();
                                VolleyLog.e("Error Hahaha: ", er.getMessage());
                            }
                        };
                        RegstaConn RcONN=new RegstaConn(nms,eml,IdS,Phon,Usrn,pwdd,responseListener);
                        RequestQueue queue= Volley.newRequestQueue(RegiSta.this);
                        //RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                        queue.add(RcONN);
                    }else {
                        progressDialog.dismiss();
                        Onyo=new AlertDialog.Builder(RegiSta.this);
                        Onyo.setMessage("Password Mismatch");
                        Onyo.setNegativeButton("Retry",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(RegiSta.this,RegiSta.class));
                            }
                        });
                        Onyo.create();
                        Onyo.show();
                    }

                }

            }
        });
    }
}
