package com.example.nicch.fliar;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicch on 9/28/17.
 */

public class RegstaConn extends StringRequest {
    private static final String Writa_Url="http://192.168.56.1/AndyBox/Brk01/NewPrjFella.php";
    private Map<String, String> params;
    public RegstaConn(String nms,String eml,int Idd,int Phon, String Usrn, String pwdd, Response.Listener<String> listener)
    {
        super(Method.POST,Writa_Url,listener,null);
        params=new HashMap<>();
        params.put("Name",nms);
        params.put("EmailAddress",eml);
        params.put("IDNO",Idd+"");
        params.put("TelePhoneNo",Phon+"");
        params.put("Username",Usrn);
        params.put("Password",pwdd);

    }
    public Map<String, String> getParams()
    {
        return params;
    }
}
