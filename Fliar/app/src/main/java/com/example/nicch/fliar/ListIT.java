package com.example.nicch.fliar;

/**
 * Created by nicch on 9/28/17.
 */

public class ListIT {
    private String Hd;
    private String Desc;
    private String Imgurl;


    public ListIT(String hd, String desc,String imgurl) {
        Hd = hd;
        Desc = desc;
        Imgurl=imgurl;
    }

    public String getHd() {
        return Hd;
    }

    public String getDesc() {
        return Desc;
    }

    public String Imgurl() {
        return Imgurl;
    }

}
