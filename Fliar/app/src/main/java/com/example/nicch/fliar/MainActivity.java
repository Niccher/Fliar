package com.example.nicch.fliar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    TextView tx;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button lg= (Button) findViewById(R.id.LgBtn);
        final Button spp= (Button) findViewById(R.id.SignUpa);

        final EditText usr,pwd;
        usr= (EditText) findViewById(R.id.usrEd);
        pwd= (EditText) findViewById(R.id.pwdEd);


        lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (usr.getText().toString().equals("") || pwd.getText().toString().equals("")) {
                    builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Activity Messed..");
                    builder.setMessage("Fill All Fields Nigga....");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertdialog = builder.create();
                    alertdialog.show();
                } else {
                    final ProgressDialog progressDialog=new ProgressDialog(MainActivity.this);
                    progressDialog.setMessage("Connecting to Servers...Please wait");

                    progressDialog.show();

                    final String usern = usr.getText().toString();
                    final String passwd = pwd.getText().toString();
                    Response.Listener<String> responselistener = new Response.Listener<String>() {

                        @Override

                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if (success) {
                                    progressDialog.dismiss();
                                    startActivity(new Intent(MainActivity.this, Heada.class));

                                } else {
                                    progressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setMessage("Wrong Credentials");
                                    builder.setNegativeButton("Retry", null);
                                    builder.create();
                                    builder.show();
                                }
                            } catch (JSONException ex) {
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                                Snackbar.make(v,ex.getMessage(),Snackbar.LENGTH_LONG).show();

                            }

                            startActivity(new Intent(MainActivity.this, Heada.class));
                        }
                    };

                    LoggAcces LogAc = new LoggAcces(usern, passwd, responselistener);
                    RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                    queue.add(LogAc);
                }
                //startActivity(new Intent(MainActivity.this, Heada.class));
            }


            });

        spp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,RegiSta.class));
            }
        });
        }
    }