package com.example.nicch.fliar;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class Heada extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heada);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        //Bundle bud=getIntent().getExtras();
        //String va=bud.getString("Counta");

        //ty.setText(va);
        //pona.setText(String.valueOf(intt.getStringExtra("Counta")));
        //nam.setText(intt.getStringExtra("Counta"));
        //String decd=(getIntent().getStringExtra("Counta"));

        Fragment frg=null;
        Class FrgCla=null;
        FrgCla=Descr.class;
        try {
            frg= (Fragment) FrgCla.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        FragmentManager fm=getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.Uwanja,frg).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.heada, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Prjs) {
            Fragment frg=null;
            Class FrgCla=null;
            FrgCla=ListaPrjs.class;
            try {
                frg= (Fragment) FrgCla.newInstance();
            }catch (Exception e){
                e.printStackTrace();
            }
            FragmentManager fm=getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.Uwanja,frg).commit();
            //startActivity(new Intent(Heada.this,Projctas.class));

        } else if (id == R.id.nav_MyPrjs) {
            Fragment frg=null;
            Class FrgCla=null;
            FrgCla=mioPrj.class;
            try {
                frg= (Fragment) FrgCla.newInstance();
            }catch (Exception e){
                e.printStackTrace();
            }
            FragmentManager fm=getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.Uwanja,frg).commit();

        } else if (id == R.id.nav_AddPrjs) {
            Fragment frg=null;
            Class FrgCla=null;
            FrgCla=AsktY.class;
            try {
                frg= (Fragment) FrgCla.newInstance();
            }catch (Exception e){
                e.printStackTrace();
            }
            FragmentManager fm=getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.Uwanja,frg).commit();//*/
            //startActivity(new Intent(Heada.this,PrjADD1.class));

        } else if (id == R.id.nav_Tweak) {
            startActivity(new Intent(Heada.this,Notiff.class));

        } else if (id == R.id.nav_Lgout) {
            startActivity(new Intent(Heada.this,MainActivity.class));
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
