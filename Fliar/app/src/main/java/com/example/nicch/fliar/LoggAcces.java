package com.example.nicch.fliar;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicch on 9/28/17.
 */

public class LoggAcces extends StringRequest {
    private static final String Log_Path="http://192.168.56.1/AndyBox/Brk01/NewUserFella.php";
    private Map<String, String> params;

    public LoggAcces(String username, String password, Response.Listener<String> listener)
    {
        super(Method.POST,Log_Path,listener,null);
        params=new HashMap<>();
        params.put("Username",username);
        params.put("Password",password);


    }
    public Map<String, String> getParams()
    {
        return params;
    }
}
