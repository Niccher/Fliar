package com.example.nicch.fliar;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sella extends Fragment {

    EditText nm,desc,itpr,cst,cont;
    TextView pat;
    Button pst;

    public Sella() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getActivity().setTitle("Item Description");

        View wv= inflater.inflate(R.layout.frag_sella, container, false);

        nm= (EditText) wv.findViewById(R.id.nms);
        desc= (EditText) wv.findViewById(R.id.itdesc);
        itpr= (EditText) wv.findViewById(R.id.itcc);
        cst= (EditText) wv.findViewById(R.id.itcs);
        cont= (EditText) wv.findViewById(R.id.mycon);

        pat= (TextView) wv.findViewById(R.id.imgpatt);

        pst= (Button) wv.findViewById(R.id.Sll);

        pst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view,"Haha",Snackbar.LENGTH_LONG).show();
            }
        });

        return wv;
    }

}
