package com.example.nicch.fliar;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class Edito extends Fragment {

    Calendar cal=new GregorianCalendar();
    EditText mun,val1,val2,val3;
    Button sav,term;
    AlertDialog.Builder Onyo;

    public Edito() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View wv= inflater.inflate(R.layout.frag_edito, container, false);

        sav=(Button) wv.findViewById(R.id.btnSav);
        term= (Button) wv.findViewById(R.id.btnTerm);
        val1= (EditText) wv.findViewById(R.id.etInp);
        val2= (EditText) wv.findViewById(R.id.etSpec);
        val3= (EditText) wv.findViewById(R.id.etCostar);
        mun= (EditText) wv.findViewById(R.id.edMun);
        int dy=cal.get(Calendar.DAY_OF_MONTH);
        int dy2=cal.get(Calendar.MONTH);
        int dy3=cal.get(Calendar.YEAR);

        mun.setEnabled(Boolean.FALSE);

        val1.requestFocus();


        mun.setText(String.valueOf(dy)+"--"+String.valueOf(dy2)+"--"+String.valueOf(dy3));
        sav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String var1,var2,var3;

                var1=String.valueOf(val1.getText());
                var2=String.valueOf(val2.getText());
                var3=String.valueOf(val3.getText());

                Onyo=new AlertDialog.Builder(getContext());
                //Onyo.setMessage("Inputs->\nSpecification->\nCost->");
                Onyo.setMessage("Inputs->"+var1+"\nSpecification->"+var2+"\nCost->"+var3);
                Onyo.setPositiveButton("Continue",null);
                Onyo.setNegativeButton("Retry",null);
                Onyo.create();
                Onyo.show();

                Toast.makeText(getContext(),"*//*",Toast.LENGTH_SHORT).show();

                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                ListaPrjs npl=new ListaPrjs();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });

        term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //startActivity(new Intent(Inpts.this,Logistics.class));
                Smok();
            }
        });


        return wv;
    }

    public void Smok(){
        Onyo=new AlertDialog.Builder(getContext());
        Onyo.setMessage("--");
        Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(getContext(),Logistics.class));
                dialogInterface.dismiss();
            }
        });
    }

}
