package com.example.nicch.fliar;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import static com.example.nicch.fliar.R.layout.frag_descr;


/**
 * A simple {@link Fragment} subclass.
 */
public class Descr extends Fragment {


    public Descr() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return inflater.inflate(frag_descr, container, false);
        View viw=inflater.inflate(R.layout.frag_descr,container,false);

        ImageButton aP,vP,sI,bI;

        aP=(ImageButton) viw.findViewById(R.id.adPrjs);
        vP=(ImageButton) viw.findViewById(R.id.vwPrjs);
        sI=(ImageButton) viw.findViewById(R.id.slItms);
        bI=(ImageButton) viw.findViewById(R.id.byItms);

        aP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                AsktY npl=new AsktY();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });

        vP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                mioPrj npl=new mioPrj();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });

        bI.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                Buya npl=new Buya();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });

        sI.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                Sella npl=new Sella();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });




        return viw;

    }

    /*@Override
    public void onClick(View vw) {
        /*switch (vw.getId()){
            case R.id.adPrjs:
                Toast.makeText(this.getContext(),"Add Projects",Toast.LENGTH_SHORT).show();
                break;
            case R.id.vwPrjs:
                Toast.makeText(this.getContext(),"Add Projects",Toast.LENGTH_SHORT).show();
                break;
            case R.id.byItms:
                Toast.makeText(this.getContext(),"Add Projects",Toast.LENGTH_SHORT).show();
                break;
            case R.id.slItms:
                Toast.makeText(this.getContext(),"Add Projects",Toast.LENGTH_SHORT).show();
                break;
        }
    }*/
}
