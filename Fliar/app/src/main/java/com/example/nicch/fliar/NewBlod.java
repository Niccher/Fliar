package com.example.nicch.fliar;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewBlod extends Fragment {
    Button PrNxt;
    AlertDialog.Builder Onyo;

    EditText pn,pus,typ,ob,pid,exDur,Exin,ExoT;

    public NewBlod() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("New Anime Project");
        View rutvw= inflater.inflate(R.layout.fragment_new_blod, container, false);

        pn= (EditText) rutvw.findViewById(R.id.etPrjN);
        pus= (EditText) rutvw.findViewById(R.id.etUsr);
        pid= (EditText) rutvw.findViewById(R.id.etPID);
        ob= (EditText) rutvw.findViewById(R.id.etObjs);
        exDur= (EditText) rutvw.findViewById(R.id.etDur);
        Exin= (EditText) rutvw.findViewById(R.id.etIpt);
        ExoT= (EditText) rutvw.findViewById(R.id.etOpts);

        //pn= (EditText) findViewById(R.id.etPrjN);

        PrNxt= (Button) rutvw.findViewById(R.id.edAddAnm);

        PrNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nm,usr,obj,ped,dur,ipt,opt;
                //int ped;

                nm=pn.getText().toString();
                usr=pus.getText().toString();
                obj=ob.getText().toString();
                ped=pid.getText().toString();
                dur=exDur.getText().toString();
                ipt=Exin.getText().toString();
                opt=ExoT.getText().toString();

                if(nm.equals("")||usr.equals("")||obj.equals("")||dur.equals("")||ipt.equals("")||opt.equals("")){
                    Onyo=new AlertDialog.Builder(getContext());
                    Onyo.setTitle("Something went wrong..");
                    Onyo.setMessage("Please fill all the fields....");
                    startActivity(new Intent(getContext(), Projctas.class));
                    /*Onyo.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });*/
                    AlertDialog AllDg=Onyo.create();
                    AllDg.show();
                }
                else{
                    final String nms=nm;
                    final String usrn=usr;
                    final String obc=obj;
                    final int durtn=Integer.parseInt(dur);
                    final String inps=ipt;
                    final int pd = Integer.parseInt(ped);
                    final String optt=opt;
                    final String typo="Animal";
                    final String STs="On";

                    final ProgressDialog progressDialog=new ProgressDialog(getContext());
                    progressDialog.setMessage("Adding Anime to Servers...Please wait");

                    progressDialog.show();

                    /*Response.Listener<String> responseListener=new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse=new JSONObject(response);
                                boolean success=jsonResponse.getBoolean("success");
                                if(success){
                                    progressDialog.dismiss();
                                    startActivity(new Intent(getContext(), Projctas.class));
                                }
                                else {
                                    progressDialog.dismiss();
                                    Onyo=new AlertDialog.Builder(getContext());
                                    Onyo.setMessage("An Error Occured");
                                    Onyo.setNegativeButton("Retry",null);
                                    Onyo.create();
                                    Onyo.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        //@Override
                        public void onErrorResponse(VolleyError er) {
                            Toast.makeText(getContext(), er.getMessage(), Toast.LENGTH_SHORT).show();
                            VolleyLog.e("Error Hahaha: ", er.getMessage());
                        }
                    };

                    AddPl RcONN=new AddPl(nms,usrn,typo,STs,obc,pd,durtn,inps,optt,responseListener);
                    RequestQueue queue= Volley.newRequestQueue(getContext());
                    queue.add(RcONN);*/
                }

                String Actn=null;
                Intent intt=new Intent();

                Bundle bun=new Bundle();

                bun.putString("My Project", Actn);

                //intt.putExtra(bun);
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                mioPrj npl=new mioPrj();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();

                //startActivity(new Intent(getContext(), Heada.class));
            }
        });

        return rutvw;
    }

}
