package com.example.nicch.fliar;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListaPrjs extends Fragment {

    private RecyclerView rcVw;
    private  RecyclerView.Adapter RcVwAd;
    private static  final String pag="";

    private List<ListIT> litss;

    public ListaPrjs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vw= inflater.inflate(R.layout.frag_lista_prjs, container, false);

        rcVw= (RecyclerView) vw.findViewById(R.id.Droppa);
        rcVw.setHasFixedSize(true);
        rcVw.setLayoutManager(new LinearLayoutManager(getContext()));

        litss=new ArrayList<>();

        DataLoader();

        return vw;
    }

    private void DataLoader(){
        final ProgressDialog prgd=new ProgressDialog(getContext());
        prgd.setMessage("Performing Fetch Action");
        prgd.show();

        StringRequest strReq=new StringRequest(Request.Method.GET, pag,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        prgd.dismiss();
                        try {
                            JSONObject JOSoB=new JSONObject(response);
                            JSONArray arr=JOSoB.getJSONArray("NIggaList");

                            for (int ii=0; ii<=arr.length();ii++){
                                JSONObject jobb=arr.getJSONObject(ii);
                                ListIT lita=new ListIT(
                                        jobb.getString("Name"),
                                        jobb.getString("About"),
                                        jobb.getString("Image")
                                );
                                litss.add(lita);
                            }

                            RcVwAd=new FellAdpter(litss,getContext());
                            rcVw.setAdapter(RcVwAd);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        prgd.dismiss();
                        Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_SHORT);
                    }
                }
        );

        RequestQueue reQQ= Volley.newRequestQueue(getContext());
        reQQ.add(strReq);
    }

}
