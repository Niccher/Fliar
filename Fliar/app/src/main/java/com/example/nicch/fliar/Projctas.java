package com.example.nicch.fliar;

import android.app.DownloadManager;
import android.app.LauncherActivity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Projctas extends AppCompatActivity {

    private RecyclerView rcVw;
    private  RecyclerView.Adapter RcVwAd;
    private static  final String pag="";

    private List<ListIT> litss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projctas);

        rcVw= (RecyclerView) findViewById(R.id.Droppa2);
        rcVw.setHasFixedSize(true);
        rcVw.setLayoutManager(new LinearLayoutManager(this));

        litss=new ArrayList<>();

        //DataLoader();

        for (int ii=0; ii<=10;ii++){
            ListIT litt= new ListIT(
                    "Nigga" + (ii+1),
                    "Dummy Text ",""
            );

            litss.add(litt);
        }

        RcVwAd=new FellAdpter(litss,this);
        rcVw.setAdapter(RcVwAd);
    }

    private void DataLoader(){
        final ProgressDialog prgd=new ProgressDialog(this);
        prgd.setMessage("Performing Fetch Action");
        prgd.show();

        StringRequest strReq=new StringRequest(Request.Method.GET, pag,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        prgd.dismiss();
                        try {
                            JSONObject JOSoB=new JSONObject(response);
                            JSONArray arr=JOSoB.getJSONArray("NIggaList");

                            for (int ii=0; ii<=arr.length();ii++){
                                JSONObject jobb=arr.getJSONObject(ii);
                                ListIT lita=new ListIT(
                                        jobb.getString("Name"),
                                        jobb.getString("About"),
                                        jobb.getString("Image")
                                );
                                litss.add(lita);
                            }

                            RcVwAd=new FellAdpter(litss,getApplicationContext());
                            rcVw.setAdapter(RcVwAd);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        prgd.dismiss();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT);
                    }
                }
        );

        RequestQueue reQQ= Volley.newRequestQueue(this);
        reQQ.add(strReq);
    }
}
