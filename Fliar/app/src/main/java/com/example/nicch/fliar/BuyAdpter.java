package com.example.nicch.fliar;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by nicch on 11/25/17.
 */

public class BuyAdpter extends RecyclerView.Adapter<BuyAdpter.ViewHolder> {

    private List<ListIT> lisIts;
    private Context cnt;

    public BuyAdpter(List<ListIT> lisIts, Context cnt) {
        this.lisIts = lisIts;
        this.cnt = cnt;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vw= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.panolbot,parent,false);
        return new ViewHolder(vw);
    }

    @Override
    public void onBindViewHolder(BuyAdpter.ViewHolder holder, final int position) {

        ListIT lit=lisIts.get(position);
        /*holder.tvHd.setText(lit.getHd());
        holder.tvHd2.setText(lit.getDesc());

        Picasso.with(cnt)
                .load(lit.Imgurl())
                .into(holder.Imv);*/

        holder.RelYt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(cnt,"Buy Product "+position,Toast.LENGTH_SHORT).show();
                //Intent Goo=new Intent(cnt,Disp01.class);
                //cnt.startActivity(Goo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lisIts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvHd,tvHd2;
        public ImageView Imv;

        public RelativeLayout RelYt;

        public ViewHolder(View itemView) {
            super(itemView);

            /*tvHd=itemView.findViewById(R.id.Lab);
            tvHd2=itemView.findViewById(R.id.Lab2);
            Imv=itemView.findViewById(R.id.ImLab);*/

            RelYt=(RelativeLayout) itemView.findViewById(R.id.botPan);
        }
    }
}
