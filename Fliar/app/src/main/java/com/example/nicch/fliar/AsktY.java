package com.example.nicch.fliar;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class AsktY extends Fragment {
Button AdPL,AdAm;

    public AsktY() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rutvw= inflater.inflate(R.layout.fragment_askt_y, container, false);
        getActivity().setTitle("Select Type");
        AdPL=rutvw.findViewById(R.id.btnFarm);
        AdAm=rutvw.findViewById(R.id.btnAnim);

        AdPL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                NewPlan npl=new NewPlan();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });

        AdAm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fmm=getFragmentManager();
                FragmentTransaction fts=fmm.beginTransaction();
                NewBlod npl=new NewBlod();
                fts.replace(R.id.Uwanja,npl);
                fts.addToBackStack(null);
                fts.commit();
            }
        });

        return rutvw;
    }

}
