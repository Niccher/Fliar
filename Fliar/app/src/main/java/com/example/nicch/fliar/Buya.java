package com.example.nicch.fliar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Buya extends Fragment {

    private RecyclerView rcVw;
    private  RecyclerView.Adapter RcVwAd;

    private List<ListIT> listITs;

    public Buya() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Purchase Item");

        View rutvw= inflater.inflate(R.layout.frag_buya, container, false);

        rcVw =(RecyclerView) rutvw.findViewById(R.id.BySlct);
        rcVw.setHasFixedSize(true);
        rcVw.setLayoutManager(new LinearLayoutManager(getContext()));

        listITs=new ArrayList<>();

        for (int i=0;i<=20;i++) {
            ListIT lit = new ListIT(
                    "Product Description" + (i + 1),
                    "Item Description ",
                    "Hahah"
            );

            listITs.add(lit);
        }

            rcVw.setAdapter(RcVwAd);
            RcVwAd =new BuyAdpter(listITs,getContext());

            rcVw.setAdapter(RcVwAd);

            return rutvw;
        }

    }

