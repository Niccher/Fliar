package com.example.nicch.fliar;


import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;

import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.Calendar;


/*/**
 * //A simple {@link Fragment} subclass.
 */
public class Notiff extends AppCompatActivity {

    Button stn,stp,cont;
    NotificationManager noffman;
    TimePicker tpcer;
    boolean ntActv=false;
    int noffIf=5;
    Calendar cal= Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_notiff);


        stn= (Button) findViewById(R.id.SrtNot);
        stp= (Button) findViewById(R.id.StpNot);
        cont= (Button) findViewById(R.id.SrtCnt1);

        stn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SrtNott();
            }
        });

        stp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StpNott();
            }
        });

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StAlm();
            }
        });

    }

    public void SrtNott(){
        NotificationCompat.Builder notfB= (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setContentTitle("Alert ON Input Filing")
                .setContentText("Fill the Day's Progress")
                .setTicker("Smart Farm")
                .setSmallIcon(R.mipmap.bang);

        Intent AcvNo=new Intent(this,Heada.class);

        TaskStackBuilder stacB=TaskStackBuilder.create(this);
        stacB.addParentStack(MainActivity.class);

        stacB.addNextIntent(AcvNo);

        PendingIntent pent=stacB.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        notfB.setContentIntent(pent);

        noffman= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        noffman.notify(noffIf,notfB.build());

        ntActv=true;
    }
    public void StpNott(){
        if (ntActv){
            noffman.cancel(noffIf);
        }
    }

    public void StAlm(){
        //Long ttcont=new GregorianCalendar().getTimeInMillis()+5000;
        //cal.set(Calendar.HOUR_OF_DAY,tpcer.getHour());
        //cal.set(Calendar.MINUTE,tpcer.getMinute());
        //int hr=tpcer.getHour();
        //int min=tpcer.getMinute();
        int ff=4;
        Long ttcont= Long.valueOf(ff);
        Intent alInt=new Intent( this,Reciva.class);

        AlarmManager alman= (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alman.set(AlarmManager.RTC,ttcont,PendingIntent.getBroadcast(this,1,alInt,PendingIntent.FLAG_UPDATE_CURRENT));
    }

}
